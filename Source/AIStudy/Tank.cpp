// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Scene"));
	
	RootComponent = BoxCollision;

	BoxCollision->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));

	ChassiMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Chassi"));
	ChassiMesh->AttachTo(BoxCollision, NAME_None, EAttachLocation::KeepRelativeOffset);//SetupAttachment(BoxCollision);

	LeftTrackMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LeftTrack"));
	LeftTrackMesh->SetupAttachment(ChassiMesh);

	RightTrackMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RightTrack"));
	RightTrackMesh->SetupAttachment(ChassiMesh);

	GearsMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Gears"));
	GearsMesh->SetupAttachment(ChassiMesh);

	TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret"));
	TurretMesh->SetupAttachment(BoxCollision);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(ChassiMesh);

	Camera->SetupAttachment(SpringArm);

	Speed = 100.0f;

	DefaultSpeed = Speed;

	SpeedBoost = Speed + 100.0f;

	TurnSpeed = 30.0f;

	ReloadTime = 2.0f;

	MaxBoostFuel = BoostFuel = 5.0f;

	MaxHP = HP = 100.0f;
}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bBoosting && BoostFuel > 0.5f) {
		DefaultSpeed = SpeedBoost;
		BoostFuel -= DeltaTime;
	}
	
	if(BoostFuel< MaxBoostFuel){
		if (BoostFuel <= 0.5f) {
			DefaultSpeed = Speed;
		}
		//refills fuel in 1/4 of a second
		BoostFuel += DeltaTime / 4;

		if (BoostFuel > MaxBoostFuel)
			BoostFuel = MaxBoostFuel;
		
	}
	
	if (!bBoosting) {
		DefaultSpeed = Speed;
	}

}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward",this,&ATank::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATank::MoveRight);

	PlayerInputComponent->BindAction("Fire", EInputEvent::IE_Pressed, this, &ATank::Fire);

	PlayerInputComponent->BindAction("Boost", EInputEvent::IE_Pressed, this, &ATank::BoostOn);

	PlayerInputComponent->BindAction("Boost", EInputEvent::IE_Released, this, &ATank::BoostOff);

}

void ATank::RotateTurret(FRotator Rotation, float DeltaSeconds)
{
	FRotator NewRotation = FMath::RInterpTo(TurretMesh->GetComponentRotation(), Rotation, DeltaSeconds, 5.0f);
	TurretMesh->SetWorldRotation(NewRotation);
}

void ATank::MoveForward(float Value)
{

	FHitResult HitResult;

	FVector DeltaLocation = FVector(GetWorld()->GetDeltaSeconds() * DefaultSpeed * Value, 0.0f, 0.0f);

	AddActorLocalOffset(DeltaLocation, true, &HitResult, ETeleportType::TeleportPhysics);
	if (Value > 0)
		MoveForwardEvent();

}

void ATank::MoveRight(float Value)
{
	FRotator DeltaRotation = FRotator(0.0f, GetWorld()->GetDeltaSeconds() * TurnSpeed * Value,0.0f );

	AddActorLocalRotation(DeltaRotation, false);

}

void ATank::Fire()
{
	
	if (BulletClass && !bReloading) {
		bReloading = true;
		
		FVector FireLocation = TurretMesh->GetSocketLocation("FireSocket");
		FRotator FireRotation = TurretMesh->GetSocketRotation("FireSocket");
		
		FActorSpawnParameters SpawnInfo;
		GetWorld()->SpawnActor<ATankBullet>(BulletClass, FireLocation, FireRotation, SpawnInfo);

		GetWorldTimerManager().SetTimer(TimerHandle_Reload, this,&ATank::Reload, ReloadTime);
		
		PlayFireEffects();

	}
}

void ATank::BoostOn()
{
	bBoosting = true;
}



void ATank::BoostOff()
{
	bBoosting = false;
}


void ATank::Reload()
{
	bReloading = false;
}

void ATank::PlayFireEffects()
{
	FireEvent();
}

