// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tank.h"
#include "EnemyTank.generated.h"

/**
 * 
 */
UCLASS()
class AISTUDY_API AEnemyTank : public ATank
{
	GENERATED_BODY()
	
	
	
public:

	virtual void Tick(float DeltaTime) override;
};
