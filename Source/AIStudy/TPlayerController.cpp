// Fill out your copyright notice in the Description page of Project Settings.

#include "TPlayerController.h"
#include "Tank.h"


void ATPlayerController::Tick(float DeltaSeconds) {

	Super::Tick(DeltaSeconds);
	APawn* Pawn = GetPawn();
	FHitResult HitResult;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, HitResult);

	if (HitResult.bBlockingHit && Pawn != nullptr) {
		
		FVector Direction = HitResult.Location - Pawn->GetActorLocation();

		
		
		FRotator Rotation = FRotator(0.0,Direction.Rotation().Yaw - 90.0f,0.0f);

		ATank* Tank = Cast<ATank>(Pawn);

		Tank->RotateTurret(Rotation,DeltaSeconds);
	
	}

}
